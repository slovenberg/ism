import numpy as np
import matplotlib.pyplot as plt


def split_polygon(
        vertex: np.ndarray,
        step: int
) -> list[np.ndarray]:
    """
        Разбиение массива прямых линий
        :param vertex: array of x- and y- coordinate of the vertix
        :param step: int, a distance between points
        :return np.ndarray массив разбиения
    """
    res = []
    i = 0
    flag = len(vertex) - 1
    while i < flag + 1:
        if i == flag:
            side = np.linspace(vertex[i], vertex[0], step)[:-1]
        else:
            side = np.linspace(vertex[i], vertex[i + 1], step)[:-1]
        res.append(side)
        i += 1
    return res


def split_arc(
        center: np.ndarray,
        angle_start: float,
        angle_end: float,
        step: int,
        radius: int
) -> np.ndarray:
    """
    Разбиение окружности
    :param angle_start: float начальный угол
    :param angle_end: float конечный угол
    :param center: list[int, int] центр точки
    :param step: int шаг
    :param radius: int радиус окружности

    :return np.ndarray массив разбиений
    """
    alpha = np.linspace(angle_start, angle_end, step)[:-1]
    res = np.column_stack((center[0] + radius * np.cos(alpha), center[1] + radius * np.sin(alpha)))
    return res[1:]


def draw_polygon(
        polygon: np.ndarray,
        color_pol: str
) -> None:
    """
    Отрисовываем массив точек
    :param polygon: np.ndarray Массив разбиений
    :param color_pol: str HEX цвет
    """
    flag = len(polygon)
    i = 0
    while i < flag:
        plt.plot(*polygon[i].T, '*', color=color_pol)
        i += 1
