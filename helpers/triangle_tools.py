import numpy as np


def find_triangle(
        triangles: np.ndarray,
        point: np.ndarray
) -> int | None:
    """
    Поиск треугольника с соответствующей точкой

    :param triangles: np.ndarray массив переданных треугольников
    :param point: np.ndarray переданная точка

    :return int | None индекс найденной точки в массиве
    """
    n = len(triangles)

    for i in range(n):
        first_criterio_x1 = triangles[i, 0, 0] - point[0, 0]
        first_criterio_x2 = triangles[i, 1, 1] - triangles[i, 0, 1]
        first_criterio_x3 = triangles[i, 1, 0] - triangles[i, 0, 0]
        first_criterio_x4 = triangles[i, 0, 1] - point[0, 1]
        first_criterio = first_criterio_x1 * first_criterio_x2 - first_criterio_x3 * first_criterio_x4

        second_criterio_x1 = triangles[i, 1, 0] - point[0, 0]
        second_criterio_x2 = triangles[i, 2, 1] - triangles[i, 1, 1]
        second_criterio_x3 = triangles[i, 2, 0] - triangles[i, 1, 0]
        second_criterio_x4 = triangles[i, 1, 1] - point[0, 1]
        second_criterio = second_criterio_x1 * second_criterio_x2 - second_criterio_x3 * second_criterio_x4

        third_criterio_x1 = triangles[i, 2, 0] - point[0, 0]
        third_criterio_x2 = triangles[i, 0, 1] - triangles[i, 2, 1]
        third_criterio_x3 = triangles[i, 0, 0] - triangles[i, 2, 0]
        third_criterio_x4 = triangles[i, 2, 1] - point[0, 1]
        third_criterio = third_criterio_x1 * third_criterio_x2 - third_criterio_x3 * third_criterio_x4
        if ((first_criterio >= 0 and second_criterio >= 0 and third_criterio >= 0) or
                (first_criterio <= 0 and second_criterio <= 0 and third_criterio <= 0)):
            return i

    return None


def interp_2d(
        nodes: np.ndarray,
        elements: np.ndarray,
        phi: np.ndarray,
        point: np.ndarray
) -> float | None:
    """
    рассчитает и вернет значение функции phi в точке (x, y), если эта точка попадает внутрь любого треугольника,
    Calculates the value of the phi function at the given point

    :param nodes np.ndarray массив координат узлов
    :param elements np.ndarray массив индексов узлов, образующих треугольники
    :param phi np.ndarray массив значений функции в узлах
    :param point np.ndarray координаты точки

    :return float
    """
    triangles = nodes[elements]
    idx = find_triangle(triangles, point)
    if idx is None:
        return None
    triangle = triangles[idx]

    sqr = np.abs(np.cross(triangle[1] - triangle[0], triangle[2] - triangle[1])) / 2
    a_i = triangle[1, 0] * triangle[2, 1] - triangle[2, 0] * triangle[1, 1]
    a_j = triangle[2, 0] * triangle[0, 1] - triangle[2, 1] * triangle[0, 0]
    a_k = triangle[0, 0] * triangle[1, 1] - triangle[1, 0] * triangle[0, 1]

    b_i = triangle[1, 1] - triangle[2, 1]
    b_j = triangle[2, 1] - triangle[0, 1]
    b_k = triangle[0, 1] - triangle[1, 1]

    c_i = triangle[2, 0] - triangle[1, 0]
    c_j = triangle[0, 0] - triangle[2, 0]
    c_k = triangle[1, 0] - triangle[0, 0]

    n_mat = np.array([
        a_i + b_i * point[0][0] + c_i * point[0][1],
        a_j + b_j * point[0][0] + c_j * point[0][1],
        a_k + b_k * point[0][0] + c_k * point[0][1]]) / (2 * sqr)

    phi_val = np.array([
        phi[elements[idx, 0]],
        phi[elements[idx, 1]],
        phi[elements[idx, 2]]
    ])

    return n_mat @ phi_val



#%%
